#! /usr/bin/env/python311

import urllib3
import pypokedex
import PIL.ImageTk
import PIL.Image
from io import BytesIO
import tkinter as tk


window = tk.Tk()
window.geometry("600x600")
window.title("NeuralNine Pokedex Tutorial")
window.config(padx=10, pady=10)

title_label = tk.Label(window, text="NeuralNine Pokedex")
title_label.config(font=("Fira Code", 32))
title_label.pack(padx=10, pady=10)

pokemon_image = tk.Label(window)
pokemon_image.pack(padx=10, pady=10)

pokemon_information = tk.Label(window)
pokemon_information.config(font=("Fira Code", 20))
pokemon_information.pack(padx=10, pady=10)

pokemon_types = tk.Label(window)
pokemon_types.config(font=("Fira Code", 20))
pokemon_types.pack(padx=10, pady=10)


def load_pokemon():
    pokemon = pypokedex.get(name=text_id_name.get(1.0, "end-1c"))

    http = urllib3.PoolManager()
    response = http.request('GET', pokemon.sprites.front.get('default'))
    image = PIL.Image.open(BytesIO(response.data))

    img = PIL.ImageTk.PhotoImage(image)
    pokemon_image.config(image=img)
    pokemon_image.image = img

    pokemon_information.config(text=f"{pokemon.dex} - {pokemon.name}")
    pokemon_types.config(text=" - ".join([t for t in pokemon.types]))


label_id_name = tk.Label(window, text="ID or Name")
label_id_name.config(font=("Fira Code", 20))
label_id_name.pack(padx=10, pady=10)

text_id_name = tk.Text(window, height=1)
text_id_name.config(font=("Fira Code", 20))
text_id_name.pack(padx=10, pady=10)

btn_load = tk.Button(window, text="Load Pokemon", command=load_pokemon)
btn_load.config(font=("Fira Code", 20))
btn_load.pack(padx=10, pady=10)

window.mainloop()


window = tk.Tk()
window.geometry("600x600")
window.title("NeuralNine Pokedex Tutorial")
window.config(padx=10, pady=10)

title_label = tk.Label(window, text="NeuralNine Pokedex")
title_label.config(font=("Fira Code", 32))
title_label.pack(padx=10, pady=10)

pokemon_image = tk.Label(window)
pokemon_image.pack(padx=10, pady=10)

pokemon_information = tk.Label(window)
pokemon_information.config(font=("Fira Code", 20))
pokemon_information.pack(padx=10, pady=10)

pokemon_types = tk.Label(window)
pokemon_types.config(font=("Fira Code", 20))
pokemon_types.pack(padx=10, pady=10)


def load_pokemon():
    pokemon = pypokedex.get(name=text_id_name.get(1.0, "end-1c"))

    http = urllib3.PoolManager()
    response = http.request('GET', pokemon.sprites.front.get('default'))
    image = PIL.Image.open(BytesIO(response.data))

    img = PIL.ImageTk.PhotoImage(image)
    pokemon_image.config(image=img)
    pokemon_image.image = img

    pokemon_information.config(text=f"{pokemon.dex} - {pokemon.name}")
    pokemon_types.config(text=" - ".join([t for t in pokemon.types]))


label_id_name = tk.Label(window, text="ID or Name")
label_id_name.config(font=("Fira Code", 20))
label_id_name.pack(padx=10, pady=10)

text_id_name = tk.Text(window, height=1)
text_id_name.config(font=("Fira Code", 20))
text_id_name.pack(padx=10, pady=10)

btn_load = tk.Button(window, text="Load Pokemon", command=load_pokemon)
btn_load.config(font=("Fira Code", 20))
btn_load.pack(padx=10, pady=10)

window.mainloop()
